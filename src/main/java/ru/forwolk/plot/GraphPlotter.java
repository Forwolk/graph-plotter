package ru.forwolk.plot;

import java.awt.image.BufferedImage;

public interface GraphPlotter {

    BufferedImage plot(Graph... graphs);
    BufferedImage plot(BufferedImage image, Graph... graphs);
}
