package ru.forwolk.plot;

public final class Constants {
    /* Ширина изображения */
    public static final int IMAGE_WIDTH = 1600;
    /* Высота изображения */
    public static final int IMAGE_HEIGHT = 900;
    /* Цвет фона изображения */
    public static final int IMAGE_BACKGROUND = 0x001f3f;

    /* Ширина графика */
    public static final int GRAPH_WIDTH = 1400;
    /* Высота графика */
    public static final int GRAPH_HEIGHT = 600;
    /* Цвет осей графика */
    public static final int GRAPH_AXES_COLOR = 0xffffff;
    /* Глубина дискретизации линий графика */
    public static final int GRAPH_DISCRETE = 50;

    /* Количество разбиений на части изображения */
    public static final int EXECUTION_PARTITIONS = 24;
    /* Количество потоков в пуле */
    public static final int EXECUTION_THREADS = 4;
}
