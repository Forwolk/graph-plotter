package ru.forwolk.plot;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws Exception {
        Map<LocalDateTime, Integer> points1 = new LinkedHashMap<>();
        Map<LocalDateTime, Integer> points2 = new LinkedHashMap<>();
        Map<LocalDateTime, Integer> points3 = new LinkedHashMap<>();

        for (int i = 0; i < 200; i ++) {
            points1.put(LocalDateTime.now().plus(i * 2, ChronoUnit.SECONDS), (int) (100 * f(i)));
            points2.put(LocalDateTime.now().plus(i * 2, ChronoUnit.SECONDS), (int) (100 * g(i)));
            points3.put(LocalDateTime.now().plus(i * 2, ChronoUnit.SECONDS), (int) (100 * h(i)));
        }

        GraphPlotter plotter = new GraphPlotterImpl();

        long start = System.currentTimeMillis();
        Graph graph1 = GraphUtils.toGraph(points1, 0xff0000, 3);
        Graph graph2 = GraphUtils.toGraph(points2, 0x00ff00, 3);
        Graph graph3 = GraphUtils.toGraph(points3, 0x0000ff, 3);
        BufferedImage graph = plotter.plot(graph1, graph2, graph3);
        long finish = System.currentTimeMillis();

        System.out.println("Time: " + new DecimalFormat("0.00").format((finish - start) / 1000d) + "s");
        ImageIO.write(graph, "png", new File("result.png"));
    }

    private static double f(int x) {
        return Math.cos(x / 10d) * x;
    }

    private static double g(int x) {
        return Math.cos(x / 30d) *Math.log(x / 10.0) * x;
    }

    private static double h(int x) {
        return (x-50) ^ 7 - 17 * x ^3 + 2 * x ^ 2 + 10;
    }
}
