package ru.forwolk.plot;

import lombok.SneakyThrows;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class GraphPlotterImpl implements GraphPlotter {

    private final ExecutorService graphPlotterExecutor = Executors.newFixedThreadPool(Constants.EXECUTION_THREADS, (r) -> {
        Thread thread = new Thread(r);
        thread.setDaemon(true);
        return thread;
    });

    @Override
    @SneakyThrows
    public BufferedImage plot(Graph... graphs) {
        BufferedImage image = prepare();
        return plot(image, graphs);
    }

    @Override
    @SneakyThrows
    public BufferedImage plot(BufferedImage image, Graph... graphs) {
        if (graphs == null || graphs.length == 0 || image == null) {
            return null;
        }

        if (image.getHeight() < Constants.GRAPH_HEIGHT || image.getWidth() < Constants.GRAPH_WIDTH) {
            throw new IllegalArgumentException("Image is smaller than graph");
        }

        LocalDateTime minTime = null;
        LocalDateTime maxTime = null;
        Integer maxValue = null;
        Integer minValue = null;

        for (Graph graph : graphs) {
            minTime = (minTime == null || graph.getMinTime().isBefore(minTime)) ? graph.getMinTime() : minTime;
            maxTime = (maxTime == null || graph.getMaxTime().isAfter(maxTime)) ? graph.getMaxTime() : maxTime;
            minValue = (minValue == null || graph.getMinValue() < minValue) ? graph.getMinValue() : minValue;
            maxValue = (maxValue == null || graph.getMaxValue() > maxValue) ? graph.getMaxValue() : maxValue;
        }


        List<Future<?>> futures = new LinkedList<>();
        for (Graph graph : graphs) {
            LocalDateTime finalMinTime = minTime;
            LocalDateTime finalMaxTime = maxTime;
            Integer finalMinValue = minValue;
            Integer finalMaxValue = maxValue;
            Future<?> future = graphPlotterExecutor.submit(() -> plot(image, graph.getTimePoints(), finalMinTime, finalMaxTime, finalMinValue, finalMaxValue, graph.getColor(), graph.getThin()));
            futures.add(future);
        }

        for (Future<?> future : futures) {
            future.get();
        }

        return image;
    }

    private BufferedImage prepare() {
        BufferedImage image = new BufferedImage(Constants.IMAGE_WIDTH, Constants.IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB);
        fillBackground(image);
        return image;
    }

    private void plot(BufferedImage image, Map<LocalDateTime, Integer> timePoints, LocalDateTime minTime, LocalDateTime maxTime, Integer minValue, Integer maxValue, int color, int thin) {
        double scaleX = getScaleX(minTime, maxTime);
        double scaleY = getScaleY(minValue, maxValue);

        int shiftX = (image.getWidth() - Constants.GRAPH_WIDTH) / 2;
        int shiftY = (image.getHeight() - Constants.GRAPH_HEIGHT) / 2;

        Integer previousX = null;
        Integer previousY = null;

        for (Map.Entry<LocalDateTime, Integer> timePoint : timePoints.entrySet()) {
            LocalDateTime time = timePoint.getKey();
            Integer value = timePoint.getValue();

            assert time.isAfter(minTime);
            assert time.isBefore(maxTime);
            assert value >= minValue;
            assert value <= maxValue;

            int x = shiftX + getGraphX(time, minTime, scaleX);
            int y = shiftY + getGraphY(value, minValue, scaleY);

            if (previousX != null) {
                drawLine(image, x, y, previousX, previousY, color, thin);
            }
            previousX = x;
            previousY = y;
        }

        drawAxes(image);
    }

    @SneakyThrows
    private void fillBackground(BufferedImage image) {
        int width = image.getWidth();
        int partitionNum = Constants.EXECUTION_PARTITIONS;

        List<Future<?>> futures = new LinkedList<>();

        for (int p = 0; p < partitionNum; p ++) {
            int startX = p * width / partitionNum;
            int finishX = (p + 1) * width / partitionNum;

            Future<?> future = graphPlotterExecutor.submit(() -> fillBackgroundPartition(image, startX, finishX));
            futures.add(future);
        }

        for (Future<?> future : futures) {
            future.get();
        }
    }

    private void fillBackgroundPartition(BufferedImage image, int startX, int finishX) {
        for (int x = startX; x < finishX; x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                image.setRGB(x, y, Constants.IMAGE_BACKGROUND);
            }
        }
    }

    private void drawAxes(BufferedImage image) {
        int shiftX = (image.getWidth() - Constants.GRAPH_WIDTH) / 2;
        int shiftY = (image.getHeight() - Constants.GRAPH_HEIGHT) / 2;

        for (int x = 0; x < Constants.GRAPH_WIDTH; x++) {
            drawPoint(image, shiftX + x, shiftY, Constants.GRAPH_AXES_COLOR, 1);
        }
        for (int y = 0; y < Constants.GRAPH_HEIGHT; y++) {
            drawPoint(image, shiftX, shiftY + y, Constants.GRAPH_AXES_COLOR, 1);
        }
    }

    private double getScaleX(LocalDateTime min, LocalDateTime max) {
        long minSeconds = min.toEpochSecond(ZoneOffset.UTC);
        long maxSeconds = max.toEpochSecond(ZoneOffset.UTC);
        return 1.0 * Constants.GRAPH_WIDTH / (maxSeconds - minSeconds);
    }

    private double getScaleY(Integer min, Integer max) {
        return 1.0 * Constants.GRAPH_HEIGHT / (max - min);
    }

    private int getGraphX(LocalDateTime time, LocalDateTime min, double scale) {
        long minSeconds = min.toEpochSecond(ZoneOffset.UTC);
        long timeSeconds = time.toEpochSecond(ZoneOffset.UTC);
        return (int) ((timeSeconds - minSeconds) * scale);
    }

    private int getGraphY(Integer value, Integer min, double scale) {
        return (int) ((value - min) * scale);
    }

    private void drawLine(BufferedImage image, int x1, int y1, int x2, int y2, int color, int thin) {
        for (int i = 0; i < Constants.GRAPH_DISCRETE; i++) {
            int x = (int) (x1 + i * 1.0 / Constants.GRAPH_DISCRETE * (x2 - x1));
            int y = (int) (y1 + i * 1.0 / Constants.GRAPH_DISCRETE * (y2 - y1));
            drawPoint(image, x, y, color, thin);
        }
    }

    private void drawPoint(BufferedImage image, int x, int y, int color, int thin) {
        for (int t = 0; t < thin; t++) {
            image.setRGB(x, image.getHeight() - 1 - (y + t), color);
        }

    }
}
