package ru.forwolk.plot;

import java.time.LocalDateTime;
import java.util.Map;

public class GraphUtils {

    public static Graph toGraph(Map<LocalDateTime, Integer> timePoints, int color, int thin) {
        if (timePoints.isEmpty()) {
            return null;
        }

        LocalDateTime minTime = null;
        LocalDateTime maxTime = null;
        Integer maxValue = null;
        Integer minValue = null;

        /* Order */
        for (Map.Entry<LocalDateTime, Integer> timePoint : timePoints.entrySet()) {
            LocalDateTime time = timePoint.getKey();
            Integer value = timePoint.getValue();
            if (maxTime == null) {
                maxTime = time;
            }
            if (minTime == null) {
                minTime = time;
            }
            if (maxValue == null) {
                maxValue = value;
            }
            if (minValue == null) {
                minValue = value;
            }

            if (time.isAfter(maxTime)) {
                maxTime = time;
            }
            if (time.isBefore(minTime)) {
                minTime = time;
            }
            if (maxValue < value) {
                maxValue = value;
            }
            if (minValue > value) {
                minValue = value;
            }
        }

        assert minValue < maxValue;
        assert minTime.isBefore(maxTime);

        Graph graph = new Graph();
        graph.setTimePoints(timePoints);
        graph.setMaxTime(maxTime);
        graph.setMinTime(minTime);
        graph.setMinValue(minValue);
        graph.setMaxValue(maxValue);
        graph.setColor(color);
        graph.setThin(thin);

        return graph;
    }
}
