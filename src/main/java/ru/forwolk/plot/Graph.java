package ru.forwolk.plot;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Map;

@Data
public class Graph {
    private int color;
    private int thin;
    private Map<LocalDateTime, Integer> timePoints;

    private int minValue;
    private int maxValue;
    private LocalDateTime minTime;
    private LocalDateTime maxTime;
}
